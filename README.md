### Comandi

Un comando è composto da:

Header + Comando + Un certo numero di byte

L'Header è il simbolo "<", cioè 0x3C.
Il comando è formato da due byte.

*Tabella*

| Comando    | Lettere associate | Comando in esadecimale | Numero di byte |
|:----------:|:-----------------:|:----------------------:|:--------------:|
| Move Right |  M R              |   0x4D52               | 4              |
| Move Left  |  M L              |   0x4D4C               | 4              |
| Move Up    |  M U              |   0x4D55               | 4              |
| Move Down  |  M D              |   0x4D44               | 4              |
| Autozero horizontal  |   Z H   |   0x5A48               | 0              |
| Autozero vertical    |   Z V   |   0x5A56               | 0              |   



### Pin Arduino

| Numero pin |  Cavo          |
|:----------:|:--------------:|
|     2      |  H Switch low  |
|     3      |  H Switch high |
|     4      |  H Step        |
|     5      |  H Dir         |
|     6      |  V Switch low  |
|     7      |  V Switch high |
|     8      |  V Step        |
|     9      |  V Dir         |

