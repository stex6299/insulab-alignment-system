import tkinter as tk

from PIL import ImageTk, Image


# Creo la finestra

window = tk.Tk()
window.title("Controllo motorini")
#window.geometry("550x650")
window.config(bg = 'white')

# Primo frame 
# ********* TELE 1 *********


cf1 = 'white'
frame = tk.Frame(master=window, bg = 'white', highlightcolor="red", highlightthickness= 5 , highlightbackground = 'red' )
frame.pack(fill=tk.X,padx=20, pady=20)


moto_label = tk.Label(frame, text = 'TELE 1', font=("Helvetica", 20), fg = 'red', bg = cf1)
moto_label.grid(row = 0, column = 0, columnspan = 2, rowspan = 2)

title_row = 2

title1_label = tk.Label(frame, text = 'Motors', font = ("Helvetica", 10, "bold"), bg = cf1)
title1_label.grid(row = title_row, column = 0)

title2_label = tk.Label(frame, text = 'Current value', font = ("Helvetica", 10, "bold"), bg = cf1)
title2_label.grid(row = title_row, column = 1)

title3_label = tk.Label(frame, text = 'Insert value', font = ("Helvetica", 10, "bold"), bg = cf1)
title3_label.grid(row = title_row, column = 2)




ver_row = 3

vert_label = tk.Label(frame, text = 'Up & Down', bg = cf1)
vert_label.grid(row =  ver_row, column = 0, padx=5, pady=5)

vert_label = tk.Label(frame, text = '0', bg =cf1)
vert_label.grid(row =  ver_row, column = 1, padx=5, pady=5)

ver_entry = tk.Entry(frame, bg = cf1)
ver_entry.grid(row= ver_row, column=2, sticky=tk.E, padx=5, pady=5)

ver_button = tk.Button(frame, text = "Go!", bg = cf1)
ver_button.grid(row= ver_row, column=3, sticky=tk.W, padx=5, pady=5)

resetver_button = tk.Button(frame, text = "Reset", bg = cf1)
resetver_button.grid(row= ver_row, column=4, sticky=tk.W, padx=5, pady=5)

hor_row = 4

hor_label = tk.Label(frame, text = 'Left & Right', bg = cf1)
hor_label.grid(row = hor_row, column = 0, padx=5, pady=5)

hor_label = tk.Label(frame, text = '0', bg = cf1)
hor_label.grid(row = hor_row, column = 1, padx=5, pady=5)

hor_entry = tk.Entry(frame, bg = cf1)
hor_entry.grid(row= hor_row, column=2, sticky=tk.E, padx=5, pady=5)

hor_button = tk.Button(frame, text = "Go!", bg = cf1)
hor_button.grid(row= hor_row, column=3, sticky=tk.W, padx=5, pady=5)

resethor_button = tk.Button(frame, text = "Reset", bg = cf1)
resethor_button.grid(row= hor_row, column=4, sticky=tk.W, padx=5, pady=5)

var = tk.IntVar()
var.set(1)
R1 = tk.Radiobutton(frame, text="Movimenti relativi", variable=var, value=1, bg = cf1)
R1.grid(row = 0, column = 4, sticky=tk.SW)

R2 = tk.Radiobutton(frame, text="Movimenti assoluti", variable=var, value=2, bg = cf1)
R2.grid(row = 1, column = 4, sticky=tk.NW)

status_label = tk.Label(frame, text = "Status", bg = cf1)
status_label.grid(row = 0, column = 2)


# Led per controllare stato porta
myCanvas = tk.Canvas(frame, width = 20, height = 20, bg = cf1, highlightbackground= cf1)  # Create 200x200 Canvas widget
myCanvas.grid(row = 1, column =2)
myOval = myCanvas.create_oval(2, 2, 15, 15, )
myCanvas.itemconfig(myOval, fill = "red")


# Primo frame 
# ********* TELE 1 *********



cf2 = 'white'

frame2 = tk.Frame(master=window,bg = cf2, highlightcolor="orange", highlightthickness= 5 , highlightbackground = 'orange')
frame2.pack(fill=tk.X,padx=20, pady=20)


moto_label = tk.Label(frame2, text = 'TELE 2', font=("Helvetica", 20), fg = 'orange', bg = cf2)
moto_label.grid(row = 0, column = 0, columnspan = 2, rowspan = 2)

title_row = 2

title1_label = tk.Label(frame2, text = 'Motors', font = ("Helvetica", 10, "bold"),bg = cf2)
title1_label.grid(row = title_row, column = 0)

title2_label = tk.Label(frame2, text = 'Current value', font = ("Helvetica", 10, "bold"),bg = cf2)
title2_label.grid(row = title_row, column = 1)

title3_label = tk.Label(frame2, text = 'Insert value', font = ("Helvetica", 10, "bold"),bg = cf2)
title3_label.grid(row = title_row, column = 2)




ver_row = 3

vert_label = tk.Label(frame2, text = 'Up & Down',bg = cf2)
vert_label.grid(row =  ver_row, column = 0, padx=5, pady=5)

vert_label = tk.Label(frame2, text = '0',bg = cf2)
vert_label.grid(row =  ver_row, column = 1, padx=5, pady=5)

ver_entry = tk.Entry(frame2,bg = cf2)
ver_entry.grid(row= ver_row, column=2, sticky=tk.E, padx=5, pady=5)

ver_button = tk.Button(frame2, text = "Go!",bg = cf2)
ver_button.grid(row= ver_row, column=3, sticky=tk.W, padx=5, pady=5)

resetver_button = tk.Button(frame2, text = "Reset",bg = cf2)
resetver_button.grid(row= ver_row, column=4, sticky=tk.W, padx=5, pady=5)

hor_row = 4

hor_label = tk.Label(frame2, text = 'Left & Right',bg = cf2)
hor_label.grid(row = hor_row, column = 0, padx=5, pady=5)

hor_label = tk.Label(frame2, text = '0',bg = cf2)
hor_label.grid(row = hor_row, column = 1, padx=5, pady=5)

hor_entry = tk.Entry(frame2,bg = cf2)
hor_entry.grid(row= hor_row, column=2, sticky=tk.E, padx=5, pady=5)

hor_button = tk.Button(frame2, text = "Go!",bg = cf2)
hor_button.grid(row= hor_row, column=3, sticky=tk.W, padx=5, pady=5)

resethor_button = tk.Button(frame2, text = "Reset",bg = cf2)
resethor_button.grid(row= hor_row, column=4, sticky=tk.W, padx=5, pady=5)

var1 = tk.IntVar()
var1.set(1)

R1 = tk.Radiobutton(frame2, text="Movimenti relativi", variable=var1, value=1,bg = cf2)
R1.grid(row = 0, column = 4, sticky=tk.SW)

R2 = tk.Radiobutton(frame2, text="Movimenti assoluti", variable=var1, value=2,bg = cf2)
R2.grid(row = 1, column = 4, sticky=tk.NW)

status_label = tk.Label(frame2, text = "Status",bg = cf2)
status_label.grid(row = 0, column = 2)


# Led per controllare stato porta
myCanvas = tk.Canvas(frame2, width = 20, height = 20,bg = cf2, highlightbackground= cf2)  # Create 200x200 Canvas widget
myCanvas.grid(row = 1, column =2)
myOval = myCanvas.create_oval(2, 2, 15, 15, )
myCanvas.itemconfig(myOval, fill = "red")



frame3 = tk.Frame(master=window, bg = 'white')
frame3.pack(fill=tk.X,padx=20, pady=20)


# img = ImageTk.PhotoImage(Image.open("orsi.jpg"))
# panel = tk.Label(frame3, image = img)
# panel.grid()

image = ImageTk.PhotoImage(file="./orsi.jpg", format="jpg")
img = tk.Label(frame3, image=image, bg = 'white')
img.pack()

window.mainloop()
