# from tkinter import *
# from PIL import ImageTk, Image



# # Create an instance of tkinter window
# win = Tk()

# # Define the geometry of the window
# win.geometry("700x500")

# # frame = Frame(win, width=600, height=400)
# # frame.pack()
# # frame.place(anchor='center', relx=0.5, rely=0.5)

# # # Create an object of tkinter ImageTk
# # img = ImageTk.PhotoImage(Image.open("orsi.jpg"))

# # # Create a Label Widget to display the text or Image
# # label = Label(frame, image = img)
# # label.pack()


# if __name__ == "__main__":
#     win.mainloop()


# #%%

# import tkinter as tk
# from PIL import ImageTk, Image
# import os

# window = tk.Tk()
# window.geometry("600x600")
# window.title("Hello TkInter!")
# #window.resizable(False, False)
# window.configure(background="white")


# def first_print():
#     text = "Hello World!"
#     text_output = tk.Label(window, text=text, fg="red", font=("Helvetica", 16))
#     text_output.grid(row=0, column=1, sticky="W")

# def second_function():
#     text = "Nuovo Messaggio! Nuova Funzione!"
#     text_output = tk.Label(window, text=text, fg="green", font=("Helvetica", 16))
#     text_output.grid(row=1, column=1, padx=50, sticky="W")

# first_button = tk.Button(text="Saluta!", command=first_print)
# first_button.grid(row=0, column=0, sticky="W")

# second_button = tk.Button(text="Seconda Funzione", command=second_function)
# second_button.grid(row=1, column=0, pady=20, sticky="W")


# if __name__ == "__main__":
#     window.mainloop()

import tkinter as tk

from PIL import ImageTk, Image


window = tk.Tk()
window.geometry("600x600")

window.resizable(True, True)
window.title("Hello TkInter!")




move_label = tk.Label(window, text="Movements")
move_label.grid(row=0, column=0, columnspan = 2, padx=5, pady=5)

up_label = tk.Label(window, text="Up")
up_label.grid(row=1, column=0, sticky=tk.W, padx=5, pady=5)

up_entry = tk.Entry(window)
up_entry.grid(row=1, column=1, sticky=tk.E, padx=5, pady=5)

down_label = tk.Label(window, text="Down")
down_label.grid(row=2, column=0, sticky=tk.W, padx=5, pady=5)

down_entry = tk.Entry(window)
down_entry.grid(row=2, column=1, sticky=tk.E, padx=5, pady=5)

right_label = tk.Label(window, text="Right")
right_label.grid(row=3, column=0, sticky=tk.W, padx=5, pady=5)

right_entry = tk.Entry(window)
right_entry.grid(row=3, column=1, sticky=tk.E, padx=5, pady=5)

left_label = tk.Label(window, text="Left")
left_label.grid(row=4, column=0, sticky=tk.W, padx=5, pady=5)

left_entry = tk.Entry(window)
left_entry.grid(row=4, column=1, sticky=tk.E, padx=5, pady=5)

current_label = tk.Label(window, text="Current Value")
current_label.grid(row=0, column=2, sticky=tk.W, padx=5, pady=5)

current_value = ["0","0","0","0"]

for idx, value in enumerate(current_value):
    l = tk.Label(window, text=value)
    l.grid(row=idx +1 , column=2, sticky=tk.W, padx=5, pady=5)


reset_label = tk.Label(window, text="Reset")
reset_label.grid(row=0, column=3, sticky=tk.W, padx=5, pady=5)

resetup_button = tk.Button(window, text = "Reset up")
resetup_button.grid(row=1, column=3, sticky=tk.W, padx=5, pady=5)

resetdown_button = tk.Button(window, text = "Reset down")
resetdown_button.grid(row=2, column=3, sticky=tk.W, padx=5, pady=5)

resetleft_button = tk.Button(window, text = "Reset left")
resetleft_button.grid(row=3, column=3, sticky=tk.W, padx=5, pady=5)

resetright_button = tk.Button(window, text = "Reset right")
resetright_button.grid(row=4, column=3, sticky=tk.W, padx=5, pady=5)



# img = ImageTk.PhotoImage(Image.open("orsi.jpg"))
# panel = tk.Label(window, image = img)
# panel.grid(row=5, column=2, sticky="W")



window.mainloop()