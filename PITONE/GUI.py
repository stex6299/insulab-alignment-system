#!/usr/bin/env python3
import tkinter as tk

from PIL import ImageTk, Image


import sys, os, re, glob, time, json
import serial
from serial import *

import inspect
import numpy as np



# *** STATUS FILE ***
configFile = r"./status.json"
hKey = "hpos"
vKey = "vpos"
pulse_per_revolutionKey = "pulse_per_revolution"
mm_per_revolutionKey = "mm_per_revolution"


# Controllo che esista il file di config
try:
    with open(configFile, "r") as f:
        settings = json.load(f)
except:
    print("Sicuro che esista il file di config?")
    sys.exit(1)
    
    
def aggiornaConfig(settings, configFile):
    """
    aggiornaConfig permette di aggiornare il dizionario delle configurazioni.
    Chiamare passando il dizionario stesso. Il file viene invece automaticamente
    pescato dalla funzione.
    
    CALL: aggiornaConfig(settings, configFile)
    """
    # Lo salvo
    try:
        with open(configFile, "w") as f:
            json.dump(settings, f, indent=4)
            

    except Exception as e: 
        print(e)
        print("Non ho salvato le informazioni nel dizionario, resteranno solo in memoria")
        
        
    tele1_v_pos.set(settings[vKey])
    tele1_h_pos.set(settings[hKey])
        
    return


pulse_per_revolution = settings[pulse_per_revolutionKey]
mm_per_revolution = settings[mm_per_revolutionKey]



# Serial configs
_baudrate = 9600
_bytesize = EIGHTBITS
_stopbits = STOPBITS_ONE
_parity = PARITY_NONE
_timeout = 2

# serial_address_Tele1 = "/dev/ttyUSB0"
serial_address_Tele1 = "COM13"

ser = serial.Serial(serial_address_Tele1, baudrate = _baudrate, 
                bytesize = _bytesize, stopbits = _stopbits, 
                parity = _parity, timeout = _timeout) 


header_char = "<"
trailer_char = ">"


# Functions
"""
Dipende dalla configurazione degli switch 1,2,3
OFF - ON - OFF = 1600 pulse per revolution
1 Revolution is always 8 mm
"""
# pulse_per_step = 1600
# mm_per_revolution = 8
def mm2step(mm: int) -> int:
    # 8mm : pulse_per_step step = INPUT (mm) : OUTPUT (step)
    return int(mm * pulse_per_revolution / mm_per_revolution) #step
def step2mm(step: int) -> int:
    # 8mm : pulse_per_step step = OUTPUT (mm) : INPUT (step)
    return mm_per_revolution * step / pulse_per_revolution
    



    

def Go_Tele1_UpDown():
    """
    Movimento Up Down primo telescopio
    """
    print("Moving 1st telescope Up/down...")
    

    try:
        # Step di movimento relativo
        mmInput = int(Tele1_UpDown.get())
        step = mm2step(mmInput)
        
        # Movimento assoluto --> Calcolo lo spostamento relativo
        if Tele1_stepType.get() == 2:
            mmStep = mmInput - settings[vKey]
            step = mm2step(mmStep)
        
        
        # Numero assoluto da usare
        absstep =int(abs(step))
        
        print(f"Relative movement of {step} step")


        # *** Costruisco il comando ***
        comando = header_char
        
        if step > 0:
            comando += "MU"
            print("Moving up")
        else:
            comando += "MD"
            print("Moving down")
        
        # Diventa un oggeto bytes
        comando = bytearray(comando.encode())
        
        comando.append(((0xff000000 & absstep) >> 24) & 0xff) # MSB
        comando.append(((0xff0000 & absstep) >> 16) & 0xff)
        comando.append(((0xff00 & absstep) >> 8) & 0xff)
        comando.append(0xff & absstep) # LSB
        trail = int.from_bytes(trailer_char.encode(), byteorder='little')
        # comando.append(trail)
    
    
        # ** Scrivo il comando ***
        print("Sto per scrivere")
        print(comando)
        
        ser.read_all() # Free buffer
        ser.write(comando)
        
        # Wait for response
        while not ser.in_waiting:
            time.sleep(.1)
        ret = int(ser.read(1))
        print(ret)
        
        
        if ret == 1:
            print("Fine corsa raggiunto")
            
            # Leggo fino ad un massimo di 8 butes(8 caratteri hex di un intero a 32 bit)
            stepsDone = int(ser.read(ser.in_waiting).decode(), 16)
            print(f"Steps done {stepsDone} steps")
            mmStepsDone = round(step2mm(stepsDone))
            print(f"Steps done {mmStepsDone} mm")
            
            # Sovrascrivo i millimetri percorsi
            mmInput = int(mmStepsDone * np.sign(step))
            
            
        # Imposto la posizione basandomi sullo step fatto
        settings[vKey] += mmInput
        # Se sono relativo, sovrascrivo
        if (Tele1_stepType.get() == 2) and (ret == 0):
            settings[vKey] = mmInput

        aggiornaConfig(settings, configFile)
        

        
        # Svuoto eventuali altre cose
        ser.read_all() # Free buffer

    
        
        if not ret: print("Done\n")
            

    except ValueError as ex:
        print("ATTENZIONE !!!")
        print(f"Stai provando a settare {Tele1_UpDown.get()} come step ...")

    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")



def Go_Tele1_LeftRight():
    """
    Movimento Left Right primo telescopio
    """
    print("Moving 1st telescope Left/Right...")

    try:
        # Step di movimento relativo
        mmInput = int(Tele1_LeftRight.get())
        step = mm2step(mmInput)
        
        # Movimento assoluto --> Calcolo lo spostamento relativo
        if Tele1_stepType.get() == 2:
            mmStep = mmInput - settings[hKey]
            step = mm2step(mmStep)

        
        # Numero assoluto da usare
        absstep = int(abs(step))
        
        print(f"Relative movement of {step} step")


        # *** Costruisco il comando ***
        comando = header_char
        
        if step > 0:
            comando += "MR"
            print("Moving right")
        else:
            comando += "ML"
            print("Moving left")
        
        # Diventa un oggeto bytes
        comando = bytearray(comando.encode())
        
        comando.append(((0xff000000 & absstep) >> 24) & 0xff) # MSB
        comando.append(((0xff0000 & absstep) >> 16) & 0xff)
        comando.append(((0xff00 & absstep) >> 8) & 0xff)
        comando.append(0xff & absstep) # LSB
        trail = int.from_bytes(trailer_char.encode(), byteorder='little')
        # comando.append(trail)
    
    
        # ** Scrivo il comando ***
        print("Sto per scrivere")
        print(comando)
        
        ser.read_all() # Free buffer
        ser.write(comando)
        
        # Wait for response
        while not ser.in_waiting:
            time.sleep(.1)
        ret = int(ser.read(1))
        print(ret)
        
        
        if ret == 1:
            print("Fine corsa raggiunto")
            
            # Leggo fino ad un massimo di 8 butes(8 caratteri hex di un intero a 32 bit)
            stepsDone = int(ser.read(ser.in_waiting).decode(), 16)
            print(f"Steps done {stepsDone} steps")
            mmStepsDone = round(step2mm(stepsDone))
            print(f"Steps done {mmStepsDone} mm")
            
            # Sovrascrivo i millimetri percorsi
            mmInput = int(mmStepsDone * np.sign(step))
            
            
        # Imposto la posizione basandomi sullo step fatto
        settings[hKey] += mmInput
        # Se sono relativo, sovrascrivo
        if (Tele1_stepType.get() == 2) and (ret == 0):
            settings[hKey] = mmInput

        aggiornaConfig(settings, configFile)

        
        # Svuoto eventuali altre cose
        ser.read_all() # Free buffer

    
        
        if not ret: print("Done\n")
        

    except ValueError as ex:
        print("ATTENZIONE !!!")
        print(f"Stai provando a settare {Tele1_LeftRight.get()} come step ...")

    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")



def Go_Tele1_Zero_H():
    """
    Auto zero Horizontal
    """
    print("Auto zero for horizontal movement...")

    try:


        # *** Costruisco il comando ***
        comando = header_char
        
        comando += "ZH"
        # comando += trailer_char
        
        # Diventa un oggeto bytes
        comando = bytearray(comando.encode())
        print(comando, type(comando))
        
    
    
        print("Sto per scrivere")
        ser.write(comando)
        
        
        # Wait for response
        while not ser.in_waiting:
            time.sleep(.1)
        ret = int(ser.read(1))
        print(ret)
        
        if ret == 1:
            print("Fine corsa raggiunto")
        
        # Svuoto eventuali altre cose
        ser.read_all() # Free buffer

        # Command executed successfully
        if not ret: 
            print("Done\n")
            
            # Set position to zero
            settings[hKey] = 0
            aggiornaConfig(settings, configFile)

    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")


def Go_Tele1_Zero_V():
    """
    Auto zero Vertical
    """
    print("Auto zero for vertical movement...")

    try:


        # *** Costruisco il comando ***
        comando = header_char
        
        comando += "ZV"
        # comando += trailer_char
        
        # Diventa un oggeto bytes
        comando = bytearray(comando.encode())
        print(comando, type(comando))
        
    
    
        print("Sto per scrivere")
        ser.write(comando)
        
        
        # Wait for response
        while not ser.in_waiting:
            time.sleep(.1)
        ret = int(ser.read(1))
        print(ret)
        
        
        # Svuoto eventuali altre cose
        ser.read_all() # Free buffer

        # Command executed successfully
        if not ret: 
            print("Done\n")
            
            # Set position to zero
            settings[vKey] = 0
            aggiornaConfig(settings, configFile)

    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")

    








# Creo la finestra

window = tk.Tk()
window.title("Controllo motorini")
#window.geometry("550x650")
window.config(bg = 'white')

# Primo frame 
# ********* TELE 1 *********

# posizioni primo telescopio
tele1_v_pos = tk.StringVar()
# tele1_v_pos.set(settings[vKey])
tele1_h_pos = tk.StringVar()
# tele1_h_pos.set(settings[hKey])

# Lo chiamo cosi setta anche le variabili
aggiornaConfig(settings, configFile)

cf1 = 'white'
frame = tk.Frame(master=window, bg = 'white', highlightcolor="red", highlightthickness= 5 , highlightbackground = 'red' )
frame.pack(fill=tk.X,padx=20, pady=20)


moto_label = tk.Label(frame, text = 'TELE 1', font=("Helvetica", 20), fg = 'red', bg = cf1)
moto_label.grid(row = 0, column = 0, columnspan = 2, rowspan = 2)

title_row = 2

title1_label = tk.Label(frame, text = 'Motors', font = ("Helvetica", 10, "bold"), bg = cf1)
title1_label.grid(row = title_row, column = 0)

title2_label = tk.Label(frame, text = 'Current value', font = ("Helvetica", 10, "bold"), bg = cf1)
title2_label.grid(row = title_row, column = 1)

title3_label = tk.Label(frame, text = 'Insert value', font = ("Helvetica", 10, "bold"), bg = cf1)
title3_label.grid(row = title_row, column = 2)




ver_row = 3

vert_label = tk.Label(frame, text = 'Up & Down', bg = cf1)
vert_label.grid(row =  ver_row, column = 0, padx=5, pady=5)


vert_label = tk.Label(frame, textvariable = tele1_v_pos, bg =cf1)
vert_label.grid(row =  ver_row, column = 1, padx=5, pady=5)

# verget_button = tk.Button(frame, text = "Get!", bg = cf1)
# verget_button.grid(row= ver_row, column=2, sticky=tk.W, padx=5, pady=5)

Tele1_UpDown = tk.StringVar()
Tele1_UpDown.set("0")
ver_entry = tk.Entry(frame, bg = cf1, textvariable=Tele1_UpDown)
ver_entry.grid(row= ver_row, column=2, sticky=tk.E, padx=5, pady=5)

ver_button = tk.Button(frame, text = "Go!", bg = cf1, command = Go_Tele1_UpDown)
ver_button.grid(row= ver_row, column=3, sticky=tk.W, padx=5, pady=5)

resetver_button = tk.Button(frame, text = "Reset", bg = cf1, command = Go_Tele1_Zero_V)
resetver_button.grid(row= ver_row, column=4, sticky=tk.W, padx=5, pady=5)

hor_row = 4

hor_label = tk.Label(frame, text = 'Left & Right', bg = cf1)
hor_label.grid(row = hor_row, column = 0, padx=5, pady=5)

hor_label = tk.Label(frame, textvariable = tele1_h_pos, bg = cf1)
hor_label.grid(row = hor_row, column = 1, padx=5, pady=5)

# horget_button = tk.Button(frame, text = "Get!", bg = cf1)
# horget_button.grid(row= hor_row, column=2, sticky=tk.W, padx=5, pady=5)

Tele1_LeftRight = tk.StringVar()
Tele1_LeftRight.set("0")
hor_entry = tk.Entry(frame, bg = cf1, textvariable = Tele1_LeftRight)
hor_entry.grid(row= hor_row, column=2, sticky=tk.E, padx=5, pady=5)

hor_button = tk.Button(frame, text = "Go!", bg = cf1, command = Go_Tele1_LeftRight)
hor_button.grid(row= hor_row, column=3, sticky=tk.W, padx=5, pady=5)

resethor_button = tk.Button(frame, text = "Reset", bg = cf1, command = Go_Tele1_Zero_H)
resethor_button.grid(row= hor_row, column=4, sticky=tk.W, padx=5, pady=5)

Tele1_stepType = tk.IntVar()
Tele1_stepType.set(1)
R1 = tk.Radiobutton(frame, text="Movimenti relativi", variable=Tele1_stepType, value=1, bg = cf1)
R1.grid(row = 0, column = 4, sticky=tk.SW)

R2 = tk.Radiobutton(frame, text="Movimenti assoluti", variable=Tele1_stepType, value=2, bg = cf1)
R2.grid(row = 1, column = 4, sticky=tk.NW)

status_label = tk.Label(frame, text = "Status", bg = cf1)
status_label.grid(row = 0, column = 2)


# Led per controllare stato porta
myCanvas = tk.Canvas(frame, width = 20, height = 20, bg = cf1, highlightbackground= cf1)  # Create 200x200 Canvas widget
myCanvas.grid(row = 1, column =2)
myOval = myCanvas.create_oval(2, 2, 15, 15, )
myCanvas.itemconfig(myOval, fill = "red")


# Secondp frame 
# ********* TELE 2 *********



cf2 = 'white'

frame2 = tk.Frame(master=window,bg = cf2, highlightcolor="orange", highlightthickness= 5 , highlightbackground = 'orange')
frame2.pack(fill=tk.X,padx=20, pady=20)


moto_label = tk.Label(frame2, text = 'TELE 2', font=("Helvetica", 20), fg = 'orange', bg = cf2)
moto_label.grid(row = 0, column = 0, columnspan = 2, rowspan = 2)

title_row = 2

title1_label = tk.Label(frame2, text = 'Motors', font = ("Helvetica", 10, "bold"),bg = cf2)
title1_label.grid(row = title_row, column = 0)

title2_label = tk.Label(frame2, text = 'Current value', font = ("Helvetica", 10, "bold"),bg = cf2)
title2_label.grid(row = title_row, column = 1)

title3_label = tk.Label(frame2, text = 'Insert value', font = ("Helvetica", 10, "bold"),bg = cf2)
title3_label.grid(row = title_row, column = 2)




ver_row = 3

vert_label = tk.Label(frame2, text = 'Up & Down',bg = cf2)
vert_label.grid(row =  ver_row, column = 0, padx=5, pady=5)

vert_label = tk.Label(frame2, text = '0',bg = cf2)
vert_label.grid(row =  ver_row, column = 1, padx=5, pady=5)

ver_entry = tk.Entry(frame2,bg = cf2)
ver_entry.grid(row= ver_row, column=2, sticky=tk.E, padx=5, pady=5)

ver_button = tk.Button(frame2, text = "Go!",bg = cf2)
ver_button.grid(row= ver_row, column=3, sticky=tk.W, padx=5, pady=5)

resetver_button = tk.Button(frame2, text = "Reset",bg = cf2)
resetver_button.grid(row= ver_row, column=4, sticky=tk.W, padx=5, pady=5)

hor_row = 4

hor_label = tk.Label(frame2, text = 'Left & Right',bg = cf2)
hor_label.grid(row = hor_row, column = 0, padx=5, pady=5)

hor_label = tk.Label(frame2, text = '0',bg = cf2)
hor_label.grid(row = hor_row, column = 1, padx=5, pady=5)

hor_entry = tk.Entry(frame2,bg = cf2)
hor_entry.grid(row= hor_row, column=2, sticky=tk.E, padx=5, pady=5)

hor_button = tk.Button(frame2, text = "Go!",bg = cf2)
hor_button.grid(row= hor_row, column=3, sticky=tk.W, padx=5, pady=5)

resethor_button = tk.Button(frame2, text = "Reset",bg = cf2)
resethor_button.grid(row= hor_row, column=4, sticky=tk.W, padx=5, pady=5)

Tele2_stepType = tk.IntVar()
Tele2_stepType.set(1)

R1 = tk.Radiobutton(frame2, text="Movimenti relativi", variable=Tele2_stepType, value=1,bg = cf2)
R1.grid(row = 0, column = 4, sticky=tk.SW)

R2 = tk.Radiobutton(frame2, text="Movimenti assoluti", variable=Tele2_stepType, value=2,bg = cf2)
R2.grid(row = 1, column = 4, sticky=tk.NW)

status_label = tk.Label(frame2, text = "Status",bg = cf2)
status_label.grid(row = 0, column = 2)


# Led per controllare stato porta
myCanvas = tk.Canvas(frame2, width = 20, height = 20,bg = cf2, highlightbackground= cf2)  # Create 200x200 Canvas widget
myCanvas.grid(row = 1, column =2)
myOval = myCanvas.create_oval(2, 2, 15, 15, )
myCanvas.itemconfig(myOval, fill = "red")



frame3 = tk.Frame(master=window, bg = 'white')
frame3.pack(fill=tk.X,padx=20, pady=20)


# img = ImageTk.PhotoImage(Image.open("orsi.jpg"))
# panel = tk.Label(frame3, image = img)
# panel.grid()

image = ImageTk.PhotoImage(file="./orsi.jpg", format="jpg")
img = tk.Label(frame3, image=image, bg = 'white')
img.pack()



def on_closing():
    try:
        if ser.is_open:
            ser.close()
    except:
        pass
    print("Bye bye")
    sys.exit(0)

window.protocol("WM_DELETE_WINDOW", on_closing)



window.mainloop()
