const int buttonPin = 7;  // Definisci il pin a cui è collegato il bottone
int lastButtonState = HIGH; // Memorizza lo stato precedente del bottone
int currentButtonState;     // Memorizza lo stato attuale del bottone

void setup() {
  Serial.begin(9600);      // Inizializza la comunicazione seriale
  pinMode(buttonPin, INPUT_PULLUP); // Configura il pin del bottone come input pullup
}

void loop() {
  currentButtonState = digitalRead(buttonPin); // Leggi lo stato attuale del bottone

  // Verifica se il bottone è stato premuto (da HIGH a LOW)
  if (currentButtonState == LOW && lastButtonState == HIGH) {
    Serial.println("Bottone premuto!"); // Stampa nel monitor seriale
  }

  lastButtonState = currentButtonState; // Aggiorna lo stato precedente del bottone
}