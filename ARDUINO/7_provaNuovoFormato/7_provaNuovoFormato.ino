/* 
HORIZONTAL

Positive direction sx --> dx

*/
const uint8_t h_switch_neg = 2; 
const uint8_t h_switch_pos = 3; 
const uint8_t h_step = 4; // Horizontal step pin
const uint8_t h_dir = 5; // Horizontal Dir pin (Potitive toward right)


/* 
VERTICAL

Positive direction sopra --> sotto

*/
const uint8_t v_switch_neg = 6; 
const uint8_t v_switch_pos = 7; 
const uint8_t v_step = 8; // Vertical step pin
const uint8_t v_dir = 9; // Vertical Dir pin (Positive toward XXXXXX)


// Global variables for position (in step)
uint32_t h_position = 2;
uint32_t v_position = 4;


// Function to perform an elementary step
const int delayus = 500;

// This function produces a single pulse
void elementary_step (uint8_t stepPin){
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(delayus);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(delayus);
}


// Step effettuati che vengono ritornati al pitone
uint32_t nr_step_stepped = 0;


// This function perform a number of steps along a given axis, toward a given direction
uint8_t do_n_steps(char axis, bool direction,  uint32_t numSteps){
    // axis: horizontal/vertical
    // direction: positive/negative
    // numSteps: how much steps


    // Fine corsa da controllare
    uint8_t switch_neg;
    uint8_t switch_pos;
    uint8_t switch_to_monitor; // L'unico che devo veramente controllare

    // dir pin
    uint8_t stepPin;
    uint8_t dirPin;

    // Position
    uint32_t *position;

    // Setto i pin in base alla direzione
    if (axis == 'h'){
        switch_neg = h_switch_neg;
        switch_pos = h_switch_pos;

        stepPin = h_step;
        dirPin = h_dir;

        // position = &h_position;
    } else if (axis == 'v'){
        switch_neg = v_switch_neg;
        switch_pos = v_switch_pos;

        stepPin = v_step;
        dirPin = v_dir;

        // position = &v_position;
    }

    // Switch da monitorare
    switch_to_monitor = (direction) ? switch_pos : switch_neg;


    // Imposto la direzione
    digitalWrite(dirPin, direction);


    for (uint32_t i = 0; i < numSteps; i++){
        if (digitalRead(switch_to_monitor) == LOW){
            // Sono a fine corsa
            nr_step_stepped = i;
            return 1;
        }

        elementary_step(stepPin);

        // Increment position
        // if (direction){
        //     (*position) ++;
        // } else  {
        //     (*position) --;
        // }

    }

    nr_step_stepped = numSteps;
    return 0;
}



/*
Dipende dalla configurazione degli switch 1,2,3
OFF - ON - OFF = 1600 pulse per revolution
1 Revolution is always 8 mm
*/
//const int pulse_per_step = 1600;
const int mm_per_revolution = 8;

// This function converts distances in millimeters into steps
/* int mm2step (int mm, int pulse_per_step = 1600){
    // 8mm : pulse_per_step step = INPUT (mm) : OUTPUT (step)
    return mm * pulse_per_step / mm_per_revolution; // step
} */



// Function to perform autozero
uint8_t auto_zero(char axis){
    uint8_t switch_neg;
    uint8_t stepPin;
    uint8_t dirPin;

    // In base all'asse considerato, imposto le variabili locali
    if (axis == 'h'){
        switch_neg = h_switch_neg;
        stepPin = h_step;
        dirPin = h_dir;
        // h_position = 0;
    } else if (axis == 'v'){
        switch_neg = v_switch_neg;
        stepPin = v_step;
        dirPin = v_dir;
        // v_position = 0;
    }

    // Vado a zero --> Direzione negativa
    digitalWrite(dirPin, LOW);

    // Finchè non tocco il finecorsa, vado a zero
    while(digitalRead(switch_neg) == HIGH){
        elementary_step(stepPin);
    }
    return 0;
}




void setup() {
    /* PIN configuration

    Driver signals = output
    Switch = Input pullup. NORMAL OPEN and COMMON has been connected. 
    Always read HIGH; if press button, became LOW
    */

    // Horizontal
    pinMode(h_step, OUTPUT);
    pinMode(h_dir, OUTPUT);
    pinMode(h_switch_neg, INPUT_PULLUP);
    pinMode(h_switch_pos, INPUT_PULLUP);
    
    // Vertical
    pinMode(v_step, OUTPUT);
    pinMode(v_dir, OUTPUT);
    pinMode(v_switch_neg, INPUT_PULLUP);
    pinMode(v_switch_pos, INPUT_PULLUP);

    // Default values
    digitalWrite(h_step, LOW);
    digitalWrite(h_dir, LOW);
    digitalWrite(v_step, LOW);
    digitalWrite(v_dir, LOW);

    // Start serial communication
    Serial.begin(9600);



}

int spostamento; // Spostamento in mm


const char header = '<';
const char trailer = '>';
// const int MAX_MESSAGE_LENGTH = 50;



// https://forum.arduino.cc/t/combining-two-chars-to-one-int/453922/2
uint32_t passo; // Passo di cui sopra, in step


void loop() {

    // Se è arrivato un messaggio seriale --> leggo il primo carattere
    if (Serial.available()) {
        uint8_t receivedChar = Serial.read();


        // Se è l'inizio di un messaggio
        if (receivedChar == header) {
            uint8_t messageIndex = 0;
            uint8_t MAX_MESSAGE_LENGTH = 50;
            uint8_t message[MAX_MESSAGE_LENGTH];

            uint8_t res = 17; // Risultato del movimento



            // while ((messageIndex < MAX_MESSAGE_LENGTH) && (receivedChar != tailer)) {
            while (messageIndex < MAX_MESSAGE_LENGTH) {
                if (Serial.available()) {
                    receivedChar = Serial.read();// & 0xff; // Ensure one single byte

                    if (messageIndex == 0) {

                        // Un messaggio M è costituito da 6 bytes
                        if (receivedChar == 'M') {
                            MAX_MESSAGE_LENGTH = 6;

                        // Un messaggio Z è costituito da 2 bytes
                        } else if (receivedChar == 'Z') {
                            MAX_MESSAGE_LENGTH = 2;                            
                        }
                    }
                    
                    // if (receivedChar == trailer) break;
                    message[messageIndex] = receivedChar;
                    messageIndex++;

                }
            } // while
            
            

            // Insert here message validation

            /*COMANDI DI AUTO-ZERO*/
            // Zero orizzontale
            if ((message[0] == 'Z') && (message[1] == 'H')) {
                res = auto_zero('h');
            }
            // Zero verticale
            if ((message[0] == 'Z') && (message[1] == 'V')) {
                res = auto_zero('v');
            }

            /*COMANDI DI MOVIMENTO CON STEP*/
            if (message[0] == 'M') {
                passo = (uint32_t) 0x1000000 * (uint32_t) message[2] + (uint32_t) 0x10000 * (uint32_t) message[3] + (uint32_t) 0x100 * (uint32_t) message[4] + (uint32_t) message[5];
            }
            if ((message[0] == 'M') && (message[1] == 'R')) {
                res = do_n_steps('h', HIGH, passo);
            }

            if ((message[0] == 'M') && (message[1] == 'L')) {
                res = do_n_steps('h', LOW, passo);
            }

            if ((message[0] == 'M') && (message[1] == 'U')) {
                res = do_n_steps('v', HIGH, passo);
            }

            if ((message[0] == 'M') && (message[1] == 'D')) {
                res = do_n_steps('v', LOW, passo);
            }

            // At the end, print the results
            Serial.print(res);

            // Se ho usato un comando di movimento e ho raggiunto il fine corsa
            if ((message[0] == 'M') && (res == 1)) {
                Serial.print(nr_step_stepped, HEX);
            }

        } // if (receivedChar == header)
    } // if (Serial.available() > 0)


} // void loop