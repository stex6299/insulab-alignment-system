/*
Script per muovere i motori del sistema di allineamento

*/

const int stepPin = 4; // Pin per il segnale di passo (STEP)
const int dirPin = 5; // Pin per il segnale di direzione (DIR)

const int switchSx = 2; // Pin per il primo interruttore fine corsa
const int switchDx = 3; // Pin per il secondo interruttore fine corsa

// Sono normalmente HIGH (INPUT_PULLUP)
// Quando leggo LOW sono a fine corsa
int switch1State = 0; // Variabile per lo stato del primo interruttore fine corsa
int switch2State = 0; // Variabile per lo stato del secondo interruttore fine corsa

void setup() {
  // Configurazione dei pin come input o output
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(switchSx, INPUT_PULLUP);
  pinMode(switchDx, INPUT_PULLUP);

  // Impostazione dei segnali di default
  digitalWrite(stepPin, LOW);
  digitalWrite(dirPin, LOW);

  // Avvia la comunicazione seriale
  Serial.begin(9600);
}

void loop() {
  // Leggi lo stato degli interruttori fine corsa
  switch1State = digitalRead(switchSx);
  switch2State = digitalRead(switchDx);

  // Leggi l'input seriale
  if (Serial.available() > 0) {
    char input = Serial.read();

    // Se l'input è 'd', impostare la direzione a destra
    if (input == 'd') {
        Serial.println("Mi muoverò a destra");
      digitalWrite(dirPin, HIGH);
      while (switch2State == HIGH) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
        switch2State = digitalRead(switchDx);
      }
    }

    // Se l'input è 's', impostare la direzione a sinistra
    else if (input == 's') {
        Serial.println("Mi muoverò a sinistra");
      digitalWrite(dirPin, LOW);
      while (switch1State == HIGH) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
        switch1State = digitalRead(switchSx);
      }
    }

    else if (input == 'x') {
        Serial.println("XXX");
      digitalWrite(dirPin, HIGH);
      for (int i=0; i < 10000; i++) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
        // switch1State = digitalRead(switchSx);
      }
    }
  }
}