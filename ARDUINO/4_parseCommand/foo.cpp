#include "foo.h"




/*
Dipende dalla configurazione degli switch 1,2,3
OFF - ON - OFF = 1600 pulse per revolution
1 Revolution is always 8 mm
*/

const int mm_per_revolution = 8;

int mm2step (int mm, int pulse_per_step = 1600){
    // 8mm : pulse_per_step step = INPUT (mm) : OUTPUT (step)
    return mm * pulse_per_step / mm_per_revolution; // step
}



