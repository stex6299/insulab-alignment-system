
// #include <foo.h>
#include "foo.h"
/* 
HORIZONTAL

Positive direction sx --> dx

*/
const uint8_t h_switch_neg = 2; 
const uint8_t h_switch_pos = 3; 
const uint8_t h_step = 4; // Horizontal step pin
const uint8_t h_dir = 5; // Horizontal Dir pin (Potitive toward right)


/* 
VERTICAL

Positive direction sopra --> sotto

*/
const uint8_t v_switch_neg = 6; 
const uint8_t v_switch_pos = 7; 
const uint8_t v_step = 8; // Vertical step pin
const uint8_t v_dir = 9; // Vertical Dir pin (Positive toward XXXXXX)



// Function to perform an elementary step
const int delayus = 500;

// This function produces a single pulse
void elementary_step (uint8_t stepPin){
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(delayus);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(delayus);
}



// This function perform a number of steps along a given axis, toward a given direction
int do_n_steps(char axis, bool direction,  int numSteps){
    // axis: horizontal/vertical
    // direction: positive/negative
    // numSteps: how much steps


    // Fine corsa da controllare
    uint8_t switch_neg;
    uint8_t switch_pos;
    uint8_t switch_to_monitor; // L'unico che devo veramente controllare

    // dir pin
    uint8_t stepPin;
    uint8_t dirPin;

    // Setto i pin in base alla direzione
    if (axis == 'h'){
        switch_neg = h_switch_neg;
        switch_pos = h_switch_pos;

        stepPin = h_step;
        dirPin = h_dir;
    } else if (axis == 'v'){
        switch_neg = v_switch_neg;
        switch_pos = v_switch_pos;

        stepPin = v_step;
        dirPin = v_dir;
    }

    // Switch da monitorare
    switch_to_monitor = (direction) ? switch_pos : switch_neg;


    // Imposto la direzione
    digitalWrite(dirPin, direction);


    for (int i = 0; i < numSteps; i++){
        if (digitalRead(switch_to_monitor) == LOW){
            // Sono a fine corsa
            return 1;
            // break;
        }
        elementary_step(stepPin);
    }

    return 0;
}





// Function to perform autozero
void auto_zero(char axis){
    uint8_t switch_neg;
    uint8_t stepPin;
    uint8_t dirPin;

    // In base all'asse considerato, imposto le variabili locali
    if (axis == 'h'){
        switch_neg = h_switch_neg;
        stepPin = h_step;
        dirPin = h_dir;
    } else if (axis == 'v'){
        switch_neg = v_switch_neg;
        stepPin = v_step;
        dirPin = v_dir;
    }

    // Vado a zero --> Direzione negativa
    digitalWrite(dirPin, LOW);

    // Finchè non tocco il finecorsa, vado a zero
    while(digitalRead(switch_neg) == HIGH){
        elementary_step(stepPin);
    }
}




void setup() {
    /* PIN configuration

    Driver signals = output
    Switch = Input pullup. NORMAL OPEN and COMMON has been connected. 
    Always read HIGH; if press button, became LOW
    */

    // Horizontal
    pinMode(h_step, OUTPUT);
    pinMode(h_dir, OUTPUT);
    pinMode(h_switch_neg, INPUT_PULLUP);
    pinMode(h_switch_pos, INPUT_PULLUP);
    
    // Vertical
    pinMode(v_step, OUTPUT);
    pinMode(v_dir, OUTPUT);
    pinMode(v_switch_neg, INPUT_PULLUP);
    pinMode(v_switch_pos, INPUT_PULLUP);

    // Default values
    digitalWrite(h_step, LOW);
    digitalWrite(h_dir, LOW);
    digitalWrite(v_step, LOW);
    digitalWrite(v_dir, LOW);

    // Start serial communication
    Serial.begin(9600);


}

int res; // Risultato del movimento
int spostamento; // Spostamento in mm

void loop() {

    if (Serial.available() > 0) {
        char input = Serial.read();

            if (input == 'r') {
                res =  do_n_steps('h', HIGH, 10000);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }
            if (input == 'R') {
                spostamento = mm2step(4);

                res =  do_n_steps('h', HIGH, spostamento);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }

            if (input == 'l') {
                res =  do_n_steps('h', LOW, 10000);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }

            if (input == 'u') {
                res =  do_n_steps('v', HIGH, 10000);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }

            if (input == 'd') {
                res =  do_n_steps('v', LOW, 10000);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }
            if (input == 'D') {
                spostamento = mm2step(4);

                res =  do_n_steps('v', LOW, spostamento);
                if (res != 0) {
                    Serial.println("Sono arrivato a fine corsa...");
                }
            }

            if (input == 'A') {
                auto_zero('h') ;           
            }
            if (input == 'a') {
                auto_zero('v') ;           
            }

    } // if (Serial.available() > 0)


}